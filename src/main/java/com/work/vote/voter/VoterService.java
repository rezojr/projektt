package com.work.vote.voter;

import com.work.vote.candidate.CandidateE;
import com.work.vote.candidate.CandidateRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class VoterService {

    private final VoterRepository voterRepository;
    private final CandidateRepository candidateRepository;

    @Transactional
    public VoterE create(VoterE voter) {
        return voterRepository.save(voter);
    }

    public void vote(Long voterId, Long candidateId) throws EntityNotFoundException {
        VoterE voter = voterRepository.findById(voterId)
                .orElseThrow(() -> new EntityNotFoundException("Voter with id " + voterId + " not found"));

        CandidateE candidate = candidateRepository.findById(candidateId)
                .orElseThrow(() -> new EntityNotFoundException("Candidate with id " + candidateId + " not found"));

        if (voter.isHasVoted()) {
            throw new IllegalStateException("Voter with id " + voterId + " has already voted");
        }

        voter.setHasVoted(true);
        voter.setCandidate(candidate);

        candidate.getVotes().add(voter);

        voterRepository.save(voter);
        candidateRepository.save(candidate);
    }
}
