package com.work.vote.voter;

import com.work.vote.commons.abstracts.BaseRepository;

public interface VoterRepository extends BaseRepository<VoterE> {
}
