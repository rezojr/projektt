package com.work.vote.voter;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/voters")
public class VoterController {

    private final VoterService voterService;

    @PostMapping
    public VoterE create(@RequestBody VoterE voter) {
        return voterService.create(voter);
    }

    @PutMapping("/{id}/vote/{candidateId}")
    public void vote(@PathVariable Long id,
                     @PathVariable Long candidateId) {
        voterService.vote(id, candidateId);
    }

}
