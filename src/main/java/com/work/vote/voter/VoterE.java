package com.work.vote.voter;

import com.work.vote.candidate.CandidateE;
import com.work.vote.commons.abstracts.IdEntity;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@Table(name = "voter")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class VoterE extends IdEntity {

    private String name;
    private boolean hasVoted;

    @ManyToOne(fetch = FetchType.LAZY)
    private CandidateE candidate;

}
