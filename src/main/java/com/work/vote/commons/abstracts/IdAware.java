package com.work.vote.commons.abstracts;

public interface IdAware {

    Long getId();

    default boolean hasId() { return getId() != null; }

}
