package com.work.vote.commons.abstracts;

import com.querydsl.core.types.EntityPath;
import com.querydsl.core.types.dsl.Expressions;
import com.querydsl.core.types.dsl.NumberPath;
import jakarta.persistence.EntityManager;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.QuerydslJpaPredicateExecutor;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;
import org.springframework.data.querydsl.EntityPathResolver;
import org.springframework.data.querydsl.SimpleEntityPathResolver;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.Optional;

@NoRepositoryBean
public class BaseRepositoryImpl<T extends IdEntity> extends SimpleJpaRepository<T, Long> implements BaseRepository<T> {

    private static final EntityPathResolver RESOLVER = SimpleEntityPathResolver.INSTANCE;
    private QuerydslJpaPredicateExecutor<T> predicateExecutor;
    private Class entityType;
    private NumberPath<Long> idPath;

    public BaseRepositoryImpl(JpaEntityInformation entityInformation, EntityManager entityManager) {
        super(entityInformation, entityManager);
        predicateExecutor = new QuerydslJpaPredicateExecutor(entityInformation, entityManager, RESOLVER, null);
        entityType = entityInformation.getJavaType();
        EntityPath<T> entityPath = SimpleEntityPathResolver.INSTANCE.createPath(entityType);
        idPath = Expressions.numberPath(entityType, entityPath, "id");
    }

    public BaseRepositoryImpl(Class<T> domainClass, EntityManager em) {
        super(domainClass, em);
    }
}
