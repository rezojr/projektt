package com.work.vote.commons.abstracts;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface BaseRepository<T extends IdEntity> extends JpaRepository<T, Long> {

    Optional<T> findById(Long id);

}
