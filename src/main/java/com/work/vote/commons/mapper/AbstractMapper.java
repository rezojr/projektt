package com.work.vote.commons.mapper;

public interface AbstractMapper<E, T> {

    T toDto(E entity);
    E toEntity(T dto);

}
