package com.work.vote.candidate;

import com.work.vote.commons.mapper.AbstractMapper;
import com.work.vote.commons.mapper.CommonMapperConfig;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.Comment;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CandidateMapperImpl implements AbstractMapper<CandidateE, CandidateDto> {

    private final CandidateRepository candidateRepository;

    @Override
    public CandidateDto toDto(CandidateE entity) {
        if (entity == null) {
            return null;
        }
        return CandidateDto.builder()
                .votes(candidateRepository.countVotesById(entity.getId()))
                .name(entity.getName())
                .build();
    }

    @Override
    public CandidateE toEntity(CandidateDto dto) {
        return null;
    }
}
