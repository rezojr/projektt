package com.work.vote.candidate;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@EqualsAndHashCode
public class CandidateDto {

    private String name;
    private Long votes;

}
