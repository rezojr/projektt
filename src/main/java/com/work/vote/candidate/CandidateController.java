package com.work.vote.candidate;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/candidates")
public class CandidateController {

    private final CandidateService candidateService;
    private final CandidateMapperImpl mapper;

    @PostMapping
    public CandidateE create(@RequestBody CandidateE candidate) {
        return candidateService.create(candidate);
    }

    @GetMapping("/{id}")
    public CandidateDto getOne(@PathVariable Long id) {
        return mapper.toDto(candidateService.findOne(id));
    }

    @GetMapping
    public List<CandidateDto> getAll() {
        List<CandidateE> candidates = candidateService.findAll();
        return candidates.stream()
                .map(candidate -> mapper.toDto(candidate))
                .collect(Collectors.toList());
    }

}
