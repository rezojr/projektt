package com.work.vote.candidate;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor
@Service
public class CandidateService {

    private final CandidateRepository candidateRepository;

    @Transactional
    public CandidateE create(CandidateE voter) {
        return candidateRepository.save(voter);
    }

    public CandidateE findOne(Long id) {
        return candidateRepository.findById(id).get();
    }

    public List<CandidateE> findAll() {
        return candidateRepository.findAll();
    }
}
