package com.work.vote.candidate;

import com.work.vote.commons.abstracts.BaseRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CandidateRepository extends BaseRepository<CandidateE> {

    @Query("SELECT COUNT(v) FROM VoterE v WHERE v.candidate.id = :candidateId")
    Long countVotesById(@Param("candidateId") Long candidateId);

}
