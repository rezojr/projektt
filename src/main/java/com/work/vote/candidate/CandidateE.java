package com.work.vote.candidate;

import com.work.vote.commons.abstracts.IdEntity;
import com.work.vote.voter.VoterE;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@Table(name = "candidate")
@Entity
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
@EqualsAndHashCode(callSuper = true)
public class CandidateE extends IdEntity {

    private String name;

    @OneToMany(mappedBy = "candidate")
    private List<VoterE> votes;

}
